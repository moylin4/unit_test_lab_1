﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LB1.Test
{
    [TestClass]
    public class Lab1
    {
        #region constants

        private const string RESULT_FORMAT_STR = "{0:0} degrees celsius";

        #endregion

        [TestMethod]
        public void output_is_blank_initially()
        {
            using (var form = new Form1())
            {
                form.Show();

                Assert.AreEqual(
                    "", form.lblResult.Text,
                    "lblResult is not blank initially"
                );
            }
        }

        [TestMethod]
        public void fahrenheit32_is_celsius0()
        {
            using (var form = new Form1())
            {
                form.Show();

                form.txtFahrenheit.Text = "32";
                form.btnConvert.PerformClick();

                Assert.AreEqual(
                    string.Format(RESULT_FORMAT_STR, 0), 
                    form.lblResult.Text, 
                    "32 fahrenheit is 0 celsius"
                );
            }
        }

        [TestMethod]
        public void fahrenheit212_is_celsius100()
        {
            using (var form = new Form1())
            {
                form.Show();

                form.txtFahrenheit.Text = "212";
                form.btnConvert.PerformClick();

                Assert.AreEqual(
                    string.Format(RESULT_FORMAT_STR, 100),
                    form.lblResult.Text,
                    "212 fahrenheit is 100 celsius"
                );
            }
        }

        [TestMethod]
        public void fahrenheitMinus40_is_celsiusMinus40()
        {
            using (var form = new Form1())
            {
                form.Show();

                form.txtFahrenheit.Text = "-40";
                form.btnConvert.PerformClick();

                Assert.AreEqual(
                    string.Format(RESULT_FORMAT_STR, -40),
                    form.lblResult.Text, 
                    "-40 fahrenheit is -40 celsius"
                );
            }
        }

        [TestMethod]
        public void accepts_decimals()
        {
            using (var form = new Form1())
            {
                form.Show();

                form.txtFahrenheit.Text = "212.000";
                form.btnConvert.PerformClick();

                Assert.AreEqual(
                    string.Format(RESULT_FORMAT_STR, 100),
                    form.lblResult.Text,
                    "Program does not work for input that contains decimals"
                );
            }
        }
    }
}
