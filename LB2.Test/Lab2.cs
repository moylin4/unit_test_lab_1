﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LB2.Test
{
    [TestClass]
    public class Lab2
    {
        [TestMethod]
        public void output_is_blank_initially()
        {
            using (var form = new Form1())
            {
                form.Show();

                Assert.AreEqual(
                    "", form.lblResult.Text,
                    "lblResult is not blank initially"
                );
            }
        }

        [TestMethod]
        public void all_zero()
        {
            using (var form = new Form1())
            {
                form.Show();

                form.txtEntry1.Text = "0";
                form.txtEntry2.Text = "0";
                form.txtEntry3.Text = "0";
                form.txtEntry4.Text = "0";
                form.txtEntry5.Text = "0";
                form.btnAverage.PerformClick();

                Assert.AreEqual(
                    "0.0", form.lblResult.Text
                );
            }
        }

        [TestMethod]
        public void entry1()
        {
            using (var form = new Form1())
            {
                form.Show();

                form.txtEntry1.Text = "5";
                form.txtEntry2.Text = "0";
                form.txtEntry3.Text = "0";
                form.txtEntry4.Text = "0";
                form.txtEntry5.Text = "0";
                form.btnAverage.PerformClick();

                Assert.AreEqual(
                    "1.0", form.lblResult.Text,
                    "entry 1 ignored"
                );
            }
        }

        [TestMethod]
        public void entry2()
        {
            using (var form = new Form1())
            {
                form.Show();

                form.txtEntry1.Text = "0";
                form.txtEntry2.Text = "10";
                form.txtEntry3.Text = "0";
                form.txtEntry4.Text = "0";
                form.txtEntry5.Text = "0";
                form.btnAverage.PerformClick();

                Assert.AreEqual(
                    "2.0", form.lblResult.Text,
                    "entry 2 ignored"
                );
            }
        }

        [TestMethod]
        public void entry3()
        {
            using (var form = new Form1())
            {
                form.Show();

                form.txtEntry1.Text = "0";
                form.txtEntry2.Text = "0";
                form.txtEntry3.Text = "15";
                form.txtEntry4.Text = "0";
                form.txtEntry5.Text = "0";
                form.btnAverage.PerformClick();

                Assert.AreEqual(
                    "3.0", form.lblResult.Text,
                    "entry 3 ignored"
                );
            }
        }

        [TestMethod]
        public void entry4()
        {
            using (var form = new Form1())
            {
                form.Show();

                form.txtEntry1.Text = "0";
                form.txtEntry2.Text = "0";
                form.txtEntry3.Text = "0";
                form.txtEntry4.Text = "20";
                form.txtEntry5.Text = "0";
                form.btnAverage.PerformClick();

                Assert.AreEqual(
                    "4.0", form.lblResult.Text,
                    "entry 4 ignored"
                );
            }
        }

        [TestMethod]
        public void entry5()
        {
            using (var form = new Form1())
            {
                form.Show();

                form.txtEntry1.Text = "0";
                form.txtEntry2.Text = "0";
                form.txtEntry3.Text = "0";
                form.txtEntry4.Text = "0";
                form.txtEntry5.Text = "25";
                form.btnAverage.PerformClick();

                Assert.AreEqual(
                    "5.0", form.lblResult.Text,
                    "entry 4 ignored"
                );
            }
        }

        [TestMethod]
        public void entry_asc_avg3()
        {
            using (var form = new Form1())
            {
                form.Show();

                form.txtEntry1.Text = "1";
                form.txtEntry2.Text = "2";
                form.txtEntry3.Text = "3";
                form.txtEntry4.Text = "4";
                form.txtEntry5.Text = "5";
                form.btnAverage.PerformClick();

                Assert.AreEqual(
                    "3.0", form.lblResult.Text,
                    "entry 4 ignored"
                );
            }
        }

        [TestMethod]
        public void entry_desc_avg3()
        {
            using (var form = new Form1())
            {
                form.Show();

                form.txtEntry1.Text = "5";
                form.txtEntry2.Text = "4";
                form.txtEntry3.Text = "3";
                form.txtEntry4.Text = "2";
                form.txtEntry5.Text = "1";
                form.btnAverage.PerformClick();

                Assert.AreEqual(
                    "3.0", form.lblResult.Text,
                    "entry 4 ignored"
                );
            }
        }

        [TestMethod]
        public void accepts_decimals()
        {
            using (var form = new Form1())
            {
                form.Show();

                form.txtEntry1.Text = "1.5";
                form.txtEntry2.Text = "2.5";
                form.txtEntry3.Text = "3.5";
                form.txtEntry4.Text = "4.5";
                form.txtEntry5.Text = "5.5";
                form.btnAverage.PerformClick();

                Assert.AreEqual(
                    "3.5", form.lblResult.Text,
                    "Program does not work for input that contains decimals"
                );
            }
        }
    }
}
