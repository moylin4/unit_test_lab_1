﻿namespace LB2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAverage = new System.Windows.Forms.Button();
            this.lblEntry1 = new System.Windows.Forms.Label();
            this.lblEntry2 = new System.Windows.Forms.Label();
            this.txtEntry1 = new System.Windows.Forms.TextBox();
            this.txtEntry2 = new System.Windows.Forms.TextBox();
            this.txtEntry3 = new System.Windows.Forms.TextBox();
            this.txtEntry4 = new System.Windows.Forms.TextBox();
            this.txtEntry5 = new System.Windows.Forms.TextBox();
            this.lblEntry3 = new System.Windows.Forms.Label();
            this.lblEntry4 = new System.Windows.Forms.Label();
            this.lblEntry5 = new System.Windows.Forms.Label();
            this.lblResult = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnAverage
            // 
            this.btnAverage.Location = new System.Drawing.Point(54, 143);
            this.btnAverage.Name = "btnAverage";
            this.btnAverage.Size = new System.Drawing.Size(100, 23);
            this.btnAverage.TabIndex = 10;
            this.btnAverage.Text = "Average";
            this.btnAverage.UseVisualStyleBackColor = true;
            this.btnAverage.Click += new System.EventHandler(this.btnAverage_Click);
            // 
            // lblEntry1
            // 
            this.lblEntry1.AutoSize = true;
            this.lblEntry1.Location = new System.Drawing.Point(32, 12);
            this.lblEntry1.Name = "lblEntry1";
            this.lblEntry1.Size = new System.Drawing.Size(16, 13);
            this.lblEntry1.TabIndex = 0;
            this.lblEntry1.Text = "1:";
            // 
            // lblEntry2
            // 
            this.lblEntry2.AutoSize = true;
            this.lblEntry2.Location = new System.Drawing.Point(32, 39);
            this.lblEntry2.Name = "lblEntry2";
            this.lblEntry2.Size = new System.Drawing.Size(16, 13);
            this.lblEntry2.TabIndex = 2;
            this.lblEntry2.Text = "2:";
            // 
            // txtEntry1
            // 
            this.txtEntry1.Location = new System.Drawing.Point(54, 9);
            this.txtEntry1.Name = "txtEntry1";
            this.txtEntry1.Size = new System.Drawing.Size(100, 20);
            this.txtEntry1.TabIndex = 1;
            // 
            // txtEntry2
            // 
            this.txtEntry2.Location = new System.Drawing.Point(54, 36);
            this.txtEntry2.Name = "txtEntry2";
            this.txtEntry2.Size = new System.Drawing.Size(100, 20);
            this.txtEntry2.TabIndex = 3;
            // 
            // txtEntry3
            // 
            this.txtEntry3.Location = new System.Drawing.Point(54, 63);
            this.txtEntry3.Name = "txtEntry3";
            this.txtEntry3.Size = new System.Drawing.Size(100, 20);
            this.txtEntry3.TabIndex = 5;
            // 
            // txtEntry4
            // 
            this.txtEntry4.Location = new System.Drawing.Point(54, 90);
            this.txtEntry4.Name = "txtEntry4";
            this.txtEntry4.Size = new System.Drawing.Size(100, 20);
            this.txtEntry4.TabIndex = 7;
            // 
            // txtEntry5
            // 
            this.txtEntry5.Location = new System.Drawing.Point(54, 117);
            this.txtEntry5.Name = "txtEntry5";
            this.txtEntry5.Size = new System.Drawing.Size(100, 20);
            this.txtEntry5.TabIndex = 9;
            // 
            // lblEntry3
            // 
            this.lblEntry3.AutoSize = true;
            this.lblEntry3.Location = new System.Drawing.Point(32, 66);
            this.lblEntry3.Name = "lblEntry3";
            this.lblEntry3.Size = new System.Drawing.Size(16, 13);
            this.lblEntry3.TabIndex = 4;
            this.lblEntry3.Text = "3:";
            // 
            // lblEntry4
            // 
            this.lblEntry4.AutoSize = true;
            this.lblEntry4.Location = new System.Drawing.Point(32, 93);
            this.lblEntry4.Name = "lblEntry4";
            this.lblEntry4.Size = new System.Drawing.Size(16, 13);
            this.lblEntry4.TabIndex = 6;
            this.lblEntry4.Text = "4:";
            // 
            // lblEntry5
            // 
            this.lblEntry5.AutoSize = true;
            this.lblEntry5.Location = new System.Drawing.Point(32, 120);
            this.lblEntry5.Name = "lblEntry5";
            this.lblEntry5.Size = new System.Drawing.Size(16, 13);
            this.lblEntry5.TabIndex = 8;
            this.lblEntry5.Text = "5:";
            // 
            // lblResult
            // 
            this.lblResult.AutoSize = true;
            this.lblResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResult.Location = new System.Drawing.Point(51, 192);
            this.lblResult.Name = "lblResult";
            this.lblResult.Size = new System.Drawing.Size(46, 16);
            this.lblResult.TabIndex = 11;
            this.lblResult.Text = "result";
            // 
            // Form1
            // 
            this.AcceptButton = this.btnAverage;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.lblResult);
            this.Controls.Add(this.lblEntry5);
            this.Controls.Add(this.lblEntry4);
            this.Controls.Add(this.lblEntry3);
            this.Controls.Add(this.txtEntry5);
            this.Controls.Add(this.txtEntry4);
            this.Controls.Add(this.txtEntry3);
            this.Controls.Add(this.txtEntry2);
            this.Controls.Add(this.txtEntry1);
            this.Controls.Add(this.lblEntry2);
            this.Controls.Add(this.lblEntry1);
            this.Controls.Add(this.btnAverage);
            this.Name = "Form1";
            this.Text = "Average";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Button btnAverage;
        public System.Windows.Forms.Label lblEntry1;
        public System.Windows.Forms.Label lblEntry2;
        public System.Windows.Forms.TextBox txtEntry1;
        public System.Windows.Forms.TextBox txtEntry2;
        public System.Windows.Forms.TextBox txtEntry3;
        public System.Windows.Forms.TextBox txtEntry4;
        public System.Windows.Forms.TextBox txtEntry5;
        public System.Windows.Forms.Label lblEntry3;
        public System.Windows.Forms.Label lblEntry4;
        public System.Windows.Forms.Label lblEntry5;
        public System.Windows.Forms.Label lblResult;
    }
}

