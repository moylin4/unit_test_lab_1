﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LB3.Test
{
    [TestClass]
    public class Lab3
    {
        [TestMethod]
        public void output_is_blank_initially()
        {
            using (var form = new Form1())
            {
                form.Show();

                Assert.AreEqual(
                    "", form.lblTotal.Text,
                    "lblTotal is not blank initially"
                );
                Assert.AreEqual(
                    "", form.lblDozen.Text,
                    "lblDozen is not blank initially"
                );
                Assert.AreEqual(
                    "", form.lblRemainder.Text,
                    "lblRemainder is not blank initially"
                );
            }
        }

        [TestMethod]
        public void all_zero()
        {
            using (var form = new Form1())
            {
                form.Show();

                form.txtEntry1.Text = "0";
                form.txtEntry2.Text = "0";
                form.txtEntry3.Text = "0";
                form.btnCalculate.PerformClick();

                Assert.AreEqual(
                    "Total: 0", form.lblTotal.Text
                );
                Assert.AreEqual(
                    "Dozen: 0", form.lblDozen.Text
                );
                Assert.AreEqual(
                    "Remainder: 0", form.lblRemainder.Text
                );
            }
        }

        [TestMethod]
        public void entry1()
        {
            using (var form = new Form1())
            {
                form.Show();

                form.txtEntry1.Text = "14";
                form.txtEntry2.Text = "0";
                form.txtEntry3.Text = "0";
                form.btnCalculate.PerformClick();

                Assert.AreEqual(
                    "Total: 14", form.lblTotal.Text,
                    "entry 1 ignored"
                );
                Assert.AreEqual(
                    "Dozen: 1", form.lblDozen.Text,
                    "entry 1 ignored"
                );
                Assert.AreEqual(
                    "Remainder: 2", form.lblRemainder.Text,
                    "entry 1 ignored"
                );
            }
        }

        [TestMethod]
        public void entry2()
        {
            using (var form = new Form1())
            {
                form.Show();

                form.txtEntry1.Text = "0";
                form.txtEntry2.Text = "27";
                form.txtEntry3.Text = "0";
                form.btnCalculate.PerformClick();

                Assert.AreEqual(
                    "Total: 27", form.lblTotal.Text,
                    "entry 2 ignored"
                );
                Assert.AreEqual(
                    "Dozen: 2", form.lblDozen.Text,
                    "entry 2 ignored"
                );
                Assert.AreEqual(
                    "Remainder: 3", form.lblRemainder.Text,
                    "entry 2 ignored"
                );
            }
        }

        [TestMethod]
        public void entry3()
        {
            using (var form = new Form1())
            {
                form.Show();

                form.txtEntry1.Text = "0";
                form.txtEntry2.Text = "0";
                form.txtEntry3.Text = "40";
                form.btnCalculate.PerformClick();

                Assert.AreEqual(
                    "Total: 40", form.lblTotal.Text,
                    "entry 3 ignored"
                );
                Assert.AreEqual(
                    "Dozen: 3", form.lblDozen.Text,
                    "entry 3 ignored"
                );
                Assert.AreEqual(
                    "Remainder: 4", form.lblRemainder.Text,
                    "entry 3 ignored"
                );
            }
        }

        [TestMethod]
        public void total07()
        {
            using (var form = new Form1())
            {
                form.Show();

                form.txtEntry1.Text = "1";
                form.txtEntry2.Text = "2";
                form.txtEntry3.Text = "4";
                form.btnCalculate.PerformClick();

                Assert.AreEqual("Total: 7", form.lblTotal.Text);
                Assert.AreEqual("Dozen: 0", form.lblDozen.Text);
                Assert.AreEqual("Remainder: 7", form.lblRemainder.Text);
            }
        }

        [TestMethod]
        public void total12()
        {
            using (var form = new Form1())
            {
                form.Show();

                form.txtEntry1.Text = "2";
                form.txtEntry2.Text = "4";
                form.txtEntry3.Text = "6";
                form.btnCalculate.PerformClick();

                Assert.AreEqual("Total: 12", form.lblTotal.Text);
                Assert.AreEqual("Dozen: 1", form.lblDozen.Text);
                Assert.AreEqual("Remainder: 0", form.lblRemainder.Text);
            }
        }

        [TestMethod]
        public void total36()
        {
            using (var form = new Form1())
            {
                form.Show();

                form.txtEntry1.Text = "11";
                form.txtEntry2.Text = "12";
                form.txtEntry3.Text = "13";
                form.btnCalculate.PerformClick();

                Assert.AreEqual("Total: 36", form.lblTotal.Text);
                Assert.AreEqual("Dozen: 3", form.lblDozen.Text);
                Assert.AreEqual("Remainder: 0", form.lblRemainder.Text);
            }
        }

        [TestMethod]
        public void total68()
        {
            using (var form = new Form1())
            {
                form.Show();

                form.txtEntry1.Text = "23";
                form.txtEntry2.Text = "20";
                form.txtEntry3.Text = "25";
                form.btnCalculate.PerformClick();

                Assert.AreEqual("Total: 68", form.lblTotal.Text);
                Assert.AreEqual("Dozen: 5", form.lblDozen.Text);
                Assert.AreEqual("Remainder: 8", form.lblRemainder.Text);
            }
        }
    }
}
