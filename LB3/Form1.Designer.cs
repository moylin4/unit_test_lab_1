﻿namespace LB3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTotal = new System.Windows.Forms.Label();
            this.lblDozen = new System.Windows.Forms.Label();
            this.lblRemainder = new System.Windows.Forms.Label();
            this.btnCalculate = new System.Windows.Forms.Button();
            this.lblEntry3 = new System.Windows.Forms.Label();
            this.txtEntry3 = new System.Windows.Forms.TextBox();
            this.txtEntry2 = new System.Windows.Forms.TextBox();
            this.txtEntry1 = new System.Windows.Forms.TextBox();
            this.lblEntry2 = new System.Windows.Forms.Label();
            this.lblEntry1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.Location = new System.Drawing.Point(33, 145);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(38, 16);
            this.lblTotal.TabIndex = 3;
            this.lblTotal.Text = "total";
            // 
            // lblDozen
            // 
            this.lblDozen.AutoSize = true;
            this.lblDozen.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDozen.Location = new System.Drawing.Point(33, 167);
            this.lblDozen.Name = "lblDozen";
            this.lblDozen.Size = new System.Drawing.Size(50, 16);
            this.lblDozen.TabIndex = 4;
            this.lblDozen.Text = "dozen";
            // 
            // lblRemainder
            // 
            this.lblRemainder.AutoSize = true;
            this.lblRemainder.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRemainder.Location = new System.Drawing.Point(33, 189);
            this.lblRemainder.Name = "lblRemainder";
            this.lblRemainder.Size = new System.Drawing.Size(78, 16);
            this.lblRemainder.TabIndex = 5;
            this.lblRemainder.Text = "remainder";
            // 
            // btnCalculate
            // 
            this.btnCalculate.Location = new System.Drawing.Point(36, 92);
            this.btnCalculate.Name = "btnCalculate";
            this.btnCalculate.Size = new System.Drawing.Size(100, 31);
            this.btnCalculate.TabIndex = 6;
            this.btnCalculate.Text = "Calculate";
            this.btnCalculate.UseVisualStyleBackColor = true;
            this.btnCalculate.Click += new System.EventHandler(this.btnCalculate_Click);
            // 
            // lblEntry3
            // 
            this.lblEntry3.AutoSize = true;
            this.lblEntry3.Location = new System.Drawing.Point(14, 69);
            this.lblEntry3.Name = "lblEntry3";
            this.lblEntry3.Size = new System.Drawing.Size(16, 13);
            this.lblEntry3.TabIndex = 14;
            this.lblEntry3.Text = "3:";
            // 
            // txtEntry3
            // 
            this.txtEntry3.Location = new System.Drawing.Point(36, 66);
            this.txtEntry3.Name = "txtEntry3";
            this.txtEntry3.Size = new System.Drawing.Size(100, 20);
            this.txtEntry3.TabIndex = 15;
            // 
            // txtEntry2
            // 
            this.txtEntry2.Location = new System.Drawing.Point(36, 39);
            this.txtEntry2.Name = "txtEntry2";
            this.txtEntry2.Size = new System.Drawing.Size(100, 20);
            this.txtEntry2.TabIndex = 13;
            // 
            // txtEntry1
            // 
            this.txtEntry1.Location = new System.Drawing.Point(36, 12);
            this.txtEntry1.Name = "txtEntry1";
            this.txtEntry1.Size = new System.Drawing.Size(100, 20);
            this.txtEntry1.TabIndex = 11;
            // 
            // lblEntry2
            // 
            this.lblEntry2.AutoSize = true;
            this.lblEntry2.Location = new System.Drawing.Point(14, 42);
            this.lblEntry2.Name = "lblEntry2";
            this.lblEntry2.Size = new System.Drawing.Size(16, 13);
            this.lblEntry2.TabIndex = 12;
            this.lblEntry2.Text = "2:";
            // 
            // lblEntry1
            // 
            this.lblEntry1.AutoSize = true;
            this.lblEntry1.Location = new System.Drawing.Point(14, 15);
            this.lblEntry1.Name = "lblEntry1";
            this.lblEntry1.Size = new System.Drawing.Size(16, 13);
            this.lblEntry1.TabIndex = 10;
            this.lblEntry1.Text = "1:";
            // 
            // Form1
            // 
            this.AcceptButton = this.btnCalculate;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(265, 240);
            this.Controls.Add(this.lblEntry3);
            this.Controls.Add(this.txtEntry3);
            this.Controls.Add(this.txtEntry2);
            this.Controls.Add(this.txtEntry1);
            this.Controls.Add(this.lblEntry2);
            this.Controls.Add(this.lblEntry1);
            this.Controls.Add(this.btnCalculate);
            this.Controls.Add(this.lblRemainder);
            this.Controls.Add(this.lblDozen);
            this.Controls.Add(this.lblTotal);
            this.Name = "Form1";
            this.Text = "Egg Counter";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public System.Windows.Forms.Label lblEntry3;
        public System.Windows.Forms.TextBox txtEntry3;
        public System.Windows.Forms.TextBox txtEntry2;
        public System.Windows.Forms.TextBox txtEntry1;
        public System.Windows.Forms.Label lblEntry2;
        public System.Windows.Forms.Label lblEntry1;
        public System.Windows.Forms.Label lblTotal;
        public System.Windows.Forms.Label lblDozen;
        public System.Windows.Forms.Label lblRemainder;
        public System.Windows.Forms.Button btnCalculate;
    }
}

